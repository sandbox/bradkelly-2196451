(function($) {
	
	$(document).ready(
		function() {	
			
		
			var submitBtn = $('#media-tab-upload #edit-submit');
			var formToken = $("[name='uploadToken']").val();
			var buildId = $("[name='form_build_id']").val();
			var fTypes=[];
			var  maxSize, maxSizeDisplay;
			try {
				fTypes = Drupal.settings.media.browser.library.file_extensions.split(" ");
			} catch (e) {}
			
			//max file size for this filed (node edit form only)
			try {
				//field name from id
				var fieldName = window.parent.Drupal.media.lastLaunchId;
				fieldArr = fieldName.replace('edit-','').split("-");
			
				fieldName="";
				for(var i=0;i<fieldArr.length-2;i++) { //trim language etc...
					fieldName+=fieldArr[i] + "_";
				}
				fieldName = fieldName.substr(0,fieldName.length-1);
				maxSize = window.parent.Drupal.settings.resumablejs[fieldName].max_resume_upload;
				maxSizeDisplay = window.parent.Drupal.settings.resumablejs[fieldName].max_resume_display;
			} catch (e) {
				console.log(e); 
			}
			
			$('#media-tab-upload .form-item-files-upload').html(
				"<a class=\"button\" id=\"jsuploadBrowser\" >Select File</a>\
				<div id=\"limitDisplay\" class=\"description\"></div>\
				<div id=\"typeDisplay\" class=\"description\"></div>\
				<div style= \"padding:10px 10px 10px 10px;\">\
				<div id=\"fileDisplay\"></div>\
				<div id=\"progressBase\" style=\"width:300px; height:10px; border:2px solid #000000; display:none;\"><div id=\"progressBar\" style=\"height:10px; width:0px; background-color:#000000;\"></div></div>\
				<div id=\"progressStatus\"></div>\
				</div>\
				");
				
			//add details
			if (fTypes.length>0) {
				$('#typeDisplay').html("Allowed file types: <strong>" + fTypes.toString() + "</strong>");
			}
			if (maxSize>0 && maxSizeDisplay) {
				$('#limitDisplay').html("Files must be less than <strong>" + maxSizeDisplay + "</strong>");
			}

			var r = new Resumable({
				query: {token: formToken, formBuildId: buildId},
				target: '/content/aicpa/?q=jsuploader',
				fileType: fTypes,
				maxFileSize: maxSize,
			});
  
			r.assignBrowse($('#jsuploadBrowser'));
			r.on('fileSuccess', function(file, ret){
					//console.log("Success");
					//console.log(ret);
				
					var fileData = $.parseJSON(ret);
					console.log(fileData.preview);
					Drupal.media.browser.selectedMedia[0]=fileData;
					Drupal.media.browser.finalizeSelection();
				
					try {
						//node edit
						window.parent.Drupal.media.jsuploadCallback(Drupal.media.browser.selectedMedia);
						
						var buttons = $(parent.window.document.body).find('#mediaBrowser').parent('.ui-dialog').find('.ui-dialog-buttonpane button');
						buttons[1].click();
					} catch (e) {
						//media admin
						parent.window.location.reload();
					}
					
			  });
			r.on('fileProgress', function(file){
				console.log(this.getSize());
				var p = Math.round(file.progress() * 100);
				$('#progressBar').css('width',p + '%');
				$('#progressStatus').html("Uploading... " + p + "%");
			  });
			r.on('fileAdded', function(file, msg){
				console.log("file selected");
				
			    $('#fileDisplay').html(file.fileName);
		
			  });
			r.on('filesAdded', function(array){
			    //console.debug(array);
			  });
			r.on('fileRetry', function(file){
			    //console.debug(file);
			  });
			r.on('fileError', function(file, message){
			    //console.debug(file, message);
			  });
			r.on('uploadStart', function(){
			    //console.debug();
			    console.log("upload started");
			  });
			r.on('complete', function(){
			    //console.debug();
			    console.log("upload complete");
			  });
			r.on('progress', function(){
			    //console.debug();
			  });
			r.on('error', function(message, file){
			    //console.debug(message, file);
			  });
			r.on('pause', function(){
			    //console.debug();
			  });
			r.on('cancel', function(){
			    //console.debug();
			  });
						
			submitBtn.click(function() {
				$('#progressBase').css("display","block");
				$(this).css('display','none');
				$('#jsuploadBrowser').css('display','none');
				 r.upload();
				return false;
			});
		}
	);
	
})(jQuery);
