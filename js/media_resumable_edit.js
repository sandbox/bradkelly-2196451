(function($) {
	Drupal.behaviors.jsuploadEdit = {
		attach: function(context, settings) {
			
			console.log(Drupal.settings);
			Drupal.media.jsuploadCallback = function (mediaFiles) {
		          
		          if (mediaFiles.length < 0) {
		            return;
		          }
		          var mediaFile = mediaFiles[0];
		          // Set the value of the filefield fid (hidden).
		        
		          var fidField = $('#' + Drupal.media.lastLaunchId + " .fid");
		      	  
		      	  if ($('#' + Drupal.media.lastLaunchId + " .media-thumbnail").length==0) {
		      	  		$('#' + Drupal.media.lastLaunchId +"-preview").prepend("<div class=\"media-item\"><div class=\"media-thumbnail\" ></div></div>");
		      	  }
		      	  var previewField = $('#' + Drupal.media.lastLaunchId + " .media-thumbnail");
		      	  
		          var removeButton = $('#' + Drupal.media.lastLaunchId + " .remove");
		          fidField.val(mediaFile.fid);
		         
		          // Set the preview field HTML.
		          previewField.html(mediaFile.preview);
		       
		          // Show the Remove button.
		          removeButton.show();
		   };
		   
		   $('.launcher').click(function() {
		   		Drupal.media.lastLaunchId = $(this).parent().attr('id');
		   });
		     
		}
	}
	
})(jQuery);
